from setuptools import setup
from os import path

here = path.abspath(path.dirname(__file__))
with open(path.join(here, "README.md")) as f:
	long_description = f.read()

setup(
	name="ralsai",
	packages=["ralsai"],
	version="1.1.0",
	description="The Rals.ai discord bot, the cutest bot around.",
	long_description=long_description,
	url="https://gitlab.com/HarleyLorenzo/rals.ai",
	author="Harley A.W. Lorenzo",
	author_email="hl1998@protonmail.com",

	classifiers=[
		"Development Status :: 3 - Alpha",
		"Natural Language :: English",
		("""License :: OSI Approved :: """
			"""GNU General Public License v3 (GPLv3)"""),
		"Operating System :: OS Independent",
		"Programming Language :: Python :: 3.5",
		"Programming Language :: Python :: 3.6",
	],

	entry_points={
		"console_scripts":[
			"ralsai = ralsai.ralsai:main",
		],
	},

	data_files=[("", ["cfg/systemd/ralsai.service"]),
	],

	install_requires=["discord.py"],

	python_requires=">=3.5, !=3.7.*",

	project_urls={
		"Source": "https://github.com/pypa/sampleproject/",
		"Bug Reports": 
			"https://gitlab.com/HarleyLorenzo/rals.ai/issues",
	},	
)

