# Rals.ai

A discord bot written in Python3 (using the discord.py) library all centered around our favorite DELTARUNE character, Ralsei!

## Getting Started

### Prerequisites

#### Dependencies
```
python 3.5, 3.6
```

```
discord.py <=0.16.0
```
#### Installation
To install Rals.ai, simply run 

```
python setup.py install 
```

in the root directory of the project. This will install an entry point called ralsai that you can then run using

```
ralsai
```

#### Config
##### Token
Rals.ai depends on a token for the bot to run. You can acquire a token through discord's developer portal. You can specify a token through the command line with --token or a file that holds the token with --tokenfile. Otherwise the bot will attempt to find the token at either /etc/ralsai/tokenfile or in its current working directory. If these don't work, it will ask for one in the console.

#### system.d Service
Rals.ai includes a system.d service as cfg/systemd/ralsai.service. You can install this in /etc/systemd/system/ if you want the systemd service, but as of right now, the service file isn't installed automatically with setup.py. 

***NOTE:*** Make sure to setup file permissions appropriately. Just a heads up so you don't introduce a security hazard ;)

## Versioning
Rals.ai uses [Semantic Versioning](https://semver.org/)

## Authors
See AUTHORS

## Contributing
See CONTRIBUTING.md

## License
See LICENSE

## Code of Conduct
See CODE_OF_CONDUCT.md
