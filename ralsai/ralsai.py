### This is the entry point for the entire bot that contains on-run code
### As well as the core of the bot itself

### Imports

# Python Standard Library
import os
import sys
import asyncio
import argparse
import time
from pathlib import Path

# Discord.py Library
import discord
from discord.ext import commands

# Rals.ai Modules
from ralsai import config 
from ralsai import commands

### Functions

def file_openable_test(directory_to_test):
	file_to_test = Path(directory_to_test)
	return file_to_test.is_file()

### Bot setup

# Perform after conection actions 
@config.client.event
async def on_ready():
	# The bot will now "play" this game on discord 
	await config.client.change_presence(game=discord.Game
		(name="Hugging those that use r.help"))
	# Output to stdout that the bot is ready!
	print("Rals.ai is ready!")


# The entry point for Rals.ai
def main():

	# Get our event loop which is needed for certain async procedures
	config.loop = asyncio.get_event_loop()

	# Parse args passed to rals.ai

	# Start the argument parser
	parser = argparse.ArgumentParser(prog="Rals.ai")
	
	# Arguments to be added
	
	# --tokenfile
	parser.add_argument("--tokenfile", 
		help = "Specify a tokenfile location",
		action = "store", 
		dest = "tokenfile")
	# --token, -t
	parser.add_argument("--token", "-t", 
		help="Specify a token directly", 
		action = "store", 
		dest = "token")

	# Begin the parsing
	args = parser.parse_args()

	# Here we obtain a token for discord 
	#TODO: In this entire section, add checks for empty tokenfiles

	#TODO: In the following section, make use of path.join()
	# If a config was specified
	if args.token:
		token = args.token
	elif args.tokenfile:
		# Check if we can open it
		directory_to_test = args.tokenfile
		if file_openable_test(directory_to_test):
			# If we can, then read the output into token
			token_file = open(directory_to_test,"r")
			token = token_file.read()
		else:
			# If we can't,
			# drop text into stdout and exit with an error
			sys.stderr.write(
				"Config file provided could not be opened!\n")
			sys.exit(-1)
	else:
		# This is the varriable that we use to house where we're testing
		directory_to_test = "/etc/ralsai/tokenfile" 
		# If one is found in /etc/rals.ai/tokenfile, proceed as normal
		if file_openable_test(directory_to_test):
			# We set token_file to open it
			token_file = open(directory_to_test,"r")
			# Then we read the string into token
			token = token_file.read()

		# If there is not a tokenfile in /etc/rals.ai/tokenfile, 
		# look in where the script is located
		else:
			# This gets the folder where our script is located
			directory_to_test = os.path.dirname(
				os.path.realpath(__file__))
			# We add /tokenfile to it to actually look for the file
			directory_to_test = directory_to_test + "/tokenfile"
	
			if file_openable_test(directory_to_test):
				# We set token_file to open it
				token_file = open(directory_to_test,"r")
				# Then we read the string into token
				token = token_file.read()
			# If nothing else works 
			# simply ask for one in the terminal
			else:
				token = input("""No tokenfile detected. """
					"""Please input a token now: """)	 

	# Actually run the bot with the token from earlier, stripping it of 
	# unnecessary characters, specifically newlines
	config.client.run(token.strip())

# If executed normally, run the main function (which starts the bot)
if __name__ == "__main__":
		main()
