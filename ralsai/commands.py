### This module holds all commands used by Rals.ai in alphabetical format

### Imports

# Python Standard Library
import asyncio

# Rals.ai Modules
from ralsai import config

# r.bakecake
@config.client.command()
async def bakecake():
	await config.client.say("""I've baked you a cake! Enjoy! :cake: """)
	
# r.despacito
@config.client.command()
async def despacito():
	await config.client.say("""ಠ_ಠ""")

# r.discord
@config.client.command()
async def discord():
	await config.client.say("""Join our discord! It is both a fun """ \
		"""place to hang out and also contains """ \
		"""Rals.ai development news and support! \n\n """ \
		"""https://discord.gg/4Vw5jEp """)

# r.donate
@config.client.command()
async def donate():
	await config.client.say("""Rals.ai takes time to develop and """ \
		"""money to host. Donations help further future updates """ \
		"""and help host it! \n\n """ \
		"""https://www.patreon.com/harleylorenzo""")

# r.dontforget
@config.client.command()
async def dontforget():
	await config.client.say("""https://www.youtube.com/""" \
		"""watch?v=zMDSET0NIyM""")

# r.flirt
@config.client.command(pass_context = True)
async def flirt(ctx):
	# Tell the sender how beautiful they are
	await config.client.say("""<@%s>, your beauty is just... """ \
		"""transcendant...""" \
		% str(ctx.message.author.id))
	# Wait 4 seconds for comedic timing
	await asyncio.sleep(4)
	# Be embarrased 
	await config.client.say("""Oh, oh dear! Wait!!""")

# r.fohed
@config.client.command()
async def fohad():
	await config.client.say("""https://www.youtube.com/""" \
		"""watch?v=qzBO0ZpgfRw""")

# r.gitlab
@config.client.command()
async def gitlab():
	await config.client.say("""https://gitlab.com/HarleyLorenzo/rals.ai""")

# r.honey
@config.client.command()
async def honey():
	await config.client.say("""*Hugs you*   Hello, Honey! :heart:""")

# r.hug 
@config.client.command()
async def hug():
	await config.client.say("""Hugs! :heart: \n """ \
		"""https://pre00.deviantart.net/2b7f/th/pre/i/2018/306/d/a/""" \
		"""hug_ralsei_by_mcwitherzberry-dcqynhd.png""") 

# r.lewd
@config.client.command()
async def lewd():
	await config.client.say("""...Please no.""")

# r.love
@config.client.command()
async def love():
	await config.client.say("""Thank you! I love you too! :heart:""")
	
# r.lullaby
@config.client.command()
async def lullaby():
	await config.client.say("""https://www.youtube.com/""" \
		"""watch?v=77Qrdyw48xw""")

# r.magic
@config.client.command()
async def magic():
	await config.client.say(("""Huh? You want to show me some magic...? """\
		"""But that's just a mirror!"""))
	await asyncio.sleep(3)
	await config.client.say("""...""")
	await asyncio.sleep(1)
	await config.client.say(("""Oh! Goodness!""" \
		"""<:ralseiBlush:507685585947983882>"""))

# r.moneyhugs
@config.client.command()
async def moneyhug():
	await config.client.say("""You don't need money """ \
		"""to get me to hug you~""")

# r.owo
@config.client.command()
async def owo():
	await config.client.say("""What's this?""")

# r.party

# Class we use to store the name list for each channel
class Party:
	def __init__(self, channel=None, name_list=None):
		self.channel = 0
		self.name_list = []


# This is a global list to save our different parties.
# It needs to be global because of asyncio being able to reference the parties
# from several different scopes.
parties = []

async def start_party(party):
	# Set our global list
	global parties
	# Start our message string
	await config.client.send_message(party.channel, 
		""":tada: WHOO! HUG PARTY! """ \
		"""USE **r.party** TO JOIN! """ \
		"""IT WILL START IN ONE MINUTE! :tada:\n""")
	# Wait a minute
	await asyncio.sleep(60)
	# Initialize party_message
	party_message = ""
	# Add everyone to the message string that joined
	for i in range(0, len(party.name_list)):
		party_message = party_message + (" :tada: <@%s>" 
				% str(party.name_list[i]))
	# Add the end of the message
	party_message = party_message + (""" :tada: \n""" \
		"""\n https://pre00.deviantart.net/2b7f/""" \
		"""th/pre/i/2018/306/d/a/hug_ralsei_by_""" \
		"""mcwitherzberry-dcqynhd.png \n""" \
		"""I :heart: you all!""")
	# Send the message
	await config.client.send_message(party.channel, party_message)
	# Find which global we are in and delete it
	for i in range(0, len(parties)):
		if parties[i].channel == party.channel:
			del parties[i]
			break

# r.party command itself
@config.client.command(pass_context = True)
async def party(ctx):
	# Set our global list
	global parties
	# Initialize our varriable used to test if there is a party
	party_started = False
	# Is there already a party going on?
	if len(parties) == 0:
		# This may look redundant, but we don't want to 
		# test an empty list so this if is here followed by an else
		# statement.  The else statement is only triggered if there is
		# a list to test.
		party_started = False
	else:
		for i in range(len(parties)):
			# Test every party's channel and see if it
			# matches ours right now
			if parties[i].channel == ctx.message.channel:
				party_started = True
				party_to_pass = parties[i]
				break

	if party_started == True:
		# Initialize our varriable to test if someone was added
		is_added = False

		for i in range (0, len(party_to_pass.name_list)):
			# Go check all names currently on the list
			if (party_to_pass.name_list[i] == \
				str(ctx.message.author.id)):
					# If they are already on the list, 
					# set is_added to true
					is_added = True
		if is_added == False:
			# If they aren't added, add the user to the list
			party_to_pass.name_list.append(
				ctx.message.author.id)	
		else: 
			# Set the channel/server it's in
			party_to_pass.party_channel = ctx.message.channel
			# Add the party starter to the list
			party_to_pass.name_list[0] = \
				ctx.message.author.id 
			# Start the task
			loop.create_task(start_party(party_to_pass))
	else:
		# If there isn't a party going on
		
		# Create a new party object
		parties.append(Party())
		# Set its values
		parties[len(parties)-1].channel = ctx.message.channel
		parties[len(parties)-1].name_list.append( \
			ctx.message.author.id)
		# Set our varriable to pass to start_party()	
		party_to_pass = parties[len(parties)-1]
		# Start start_party through asyncio
		config.loop.create_task(start_party(party_to_pass))

# r.pat
@config.client.command()
async def pat():
	await config.client.say("""Oh! Uhhh, Thanks!""" \
		"""<:ralseiBlush:507685585947983882>""")

# Ping
@config.client.command()
async def ping():
	await config.client.say("""Pong!""")

# r.police
@config.client.command()
async def police():
	await config.client.say(("""PUT YOUR HANDS IN THE AIR! """ \
		"""*Points stuffed animal*"""))


#TODO: Create a string that both praise commands use to pythonize it

# r.PRAISE
@config.client.command()
async def PRAISE():
	await config.client.say(("""P R A I S E  T H E  F L O O F """ \
	"""<:ralseiBlush:507685585947983882>"""))
			
# r.praise	
@config.client.command()
async def praise():
	await config.client.say(("""P R A I S E  T H E  F L O O F """ \
	"""<:ralseiBlush:507685585947983882>"""))

# r.ruffle
@config.client.command(pass_context = True)
async def ruffle(ctx):
	await config.client.say("""<@%s> lovingly ruffled the floof.""" % \
		str(ctx.message.author.id))
	await asyncio.sleep(2)
	await config.client.say("""<:ralseiBlush:507685585947983882>""" \
		"""* Oh, haha... :heart:""")
	
# r.tail
@config.client.command()
async def tail():
	await config.client.say("""Don't lewd the floof""")
